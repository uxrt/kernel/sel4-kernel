<!--
    Copyright 2014, General Dynamics C4 Systems

    SPDX-License-Identifier: GPL-2.0-only
-->

# Known caveats in the UX/RT L4 API and implementation

## Implementation Correctness

Verification is currently broken. At some point limited verfication may be addedback, probably only of worst-case timing. Verification of the kernel alone with 
regards to security isn't particularly useful when much of security is 
implemented by user code that is too complex to feasibly verify.


## IPC buffer in globals frame may be stale

When a thread invokes its own TCB object to (re-)register its IPC buffer and
the thread is re-scheduled immediately, the userland IPC buffer pointer in the
globals frame may still show the old value. It is updated on the next thread
switch.


## Re-using Address Spaces (ARM and x86):

Before an ASID/page directory/page table can be reused, all frame caps
installed in it should be revoked. The kernel will not do this automatically
for the user.

If, for instance, page cap c is installed in the address space denoted by a
page directory under ASID A, and the page directory is subsequently revoked or
deleted, and then a new page directory is installed under that same ASID A,
the page cap c will still retain some authority in the new page directory,
even though the user intention might be to run the new page directory under a
new security context. The authority retained is to perform the unmap operation
on the page the cap c refers to.
